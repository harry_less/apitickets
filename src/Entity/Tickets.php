<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketsRepository")
 */
class Tickets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dateTicket;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descriptionTicket;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $severiteTicket;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDateTicket(): ?string
    {
        return $this->dateTicket;
    }

    public function setDateTicket(string $dateTicket): self
    {
        $this->dateTicket = $dateTicket;

        return $this;
    }

    public function getDescriptionTicket(): ?string
    {
        return $this->descriptionTicket;
    }

    public function setDescriptionTicket(string $descriptionTicket): self
    {
        $this->descriptionTicket = $descriptionTicket;

        return $this;
    }

    public function getSeveriteTicket(): ?string
    {
        return $this->severiteTicket;
    }

    public function setSeveriteTicket(string $severiteTicket): self
    {
        $this->severiteTicket = $severiteTicket;

        return $this;
    }
}
