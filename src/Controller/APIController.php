<?php

namespace App\Controller;

use App\Repository\TicketsRepository;
use App\Entity\Tickets;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/", name="a_p_i")
 */

class APIController extends AbstractController
{
    /**
     * @Route("/tickets/liste", name="liste")
     */
    public function liste(TicketsRepository $ticketsRepo)
    {
        // On récupère la liste des articles
        $tickets = $ticketsRepo->findAll();

        // On spécifie qu'on utilise l'encodeur JSON
        $encoders = [new JsonEncoder()];

        // On instancie le "normaliseur" pour convertir la collection en tableau
        $normalizers = [new ObjectNormalizer()];

        // On instancie le convertisseur
        $serializer = new Serializer($normalizers, $encoders);

        // On convertit en json
        $jsonContent = $serializer->serialize($tickets, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // On instancie la réponse
        $response = new Response($jsonContent);

        // On ajoute l'entête HTTP
        $response->headers->set('Content-Type', 'application/json');


        $donnees = [];
        $tab = json_decode($jsonContent);
        foreach ($tab as $key => $value) {
          $donnees[]["data"] = ["id" => $tab[$key]->id, "desq" => $tab[$key]->descriptionTicket,
           "sev" => $tab[$key]->severiteTicket,"date" => $tab[$key]->dateTicket];
        }
        return $this->render('listTickets.html.twig',['donnees' => $donnees]);
    }



    /**
     * @Route("/tickets/ajout", name="ajout")
     */
    public function addTicket(Request $request)
    {
           // On instancie un nouvel article
           $ticket = new Tickets();

           // On décode les données envoyées
           $donnees = json_decode($request->getContent());

           // On hydrate l'objet
           $ticket->setDescriptionTicket($request->request->get('desc'));
           $date = new \DateTime('now');
           $ticket->setDateTicket($date->format('Y-m-d H:i'));
           $ticket->setSeveriteTicket($request->request->get('severite'));

           // On sauvegarde en base
           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($ticket);
           $entityManager->flush();

           //On retourne la confirmation

           return $this->render('validationTickets.html.twig');

    }

    /**
    * @Route("/tickets/delete/{id}", name="supprime")
    */
    public function removeTicket($id, TicketsRepository $ticketsRepo)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entity = $ticketsRepo->findOneBy(Array("id"=>$id));
        if ($entity) {
          $entityManager->remove($entity);
          $entityManager->flush();
        }

        return $this->render('validationTickets.html.twig');

    }


     /**
     * @Route("/tickets/modifer/{id}", name="edit")
     */
     public function editTicket($id, TicketsRepository $ticketsRepo){
       $entityManager = $this->getDoctrine()->getManager();
       $entity = $ticketsRepo->findOneBy(Array("id"=>$id));
       if ($entity) {
         $donnees =  ["id" => $entity->getId(), "desq" => $entity->getDescriptionTicket(),
          "sev" => $entity->getSeveriteTicket()];
         return $this->render('updateTickets.html.twig',["donnees" => $donnees]);
       }
       return new Response('Failed', 404);
     }


    /**
     * @Route("/tickets/update/{id}", name="modif")
     */
    public function updateTicket($id,Request $request, TicketsRepository $ticketsRepo)
    {

            // On décode les données envoyées
            $donnees = json_decode($request->getContent());

            $entityManager = $this->getDoctrine()->getManager();
            // $entity = $entityManager->getRepository->find($id);
            $entity = $ticketsRepo->findOneBy(Array("id"=>$id));

            // On hydrate l'objet
            $entity->setDescriptionTicket($request->request->get('desc'));
            $date = new \DateTime('now');
            $entity->setDateTicket($date->format('Y-m-d H:i'));
            $entity->setSeveriteTicket($request->request->get('severite'));


            // On sauvegarde en base
            $entityManager->flush();

            // On retourne la confirmation
            return $this->render('validationTickets.html.twig');


    }
}
